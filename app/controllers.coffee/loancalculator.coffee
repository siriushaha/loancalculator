numeral = require('numeral')
numberMonths = 36
interestRate = 6.0
loanAmount = null

#$.loancalculatorWin.open();
init = ->
  Alloy.Globals.loantermSlider = $.loantermSlider
  $.loanamountLbl.setText 'Loan Amount ($):'
  $.loanrateLbl.setText 'Interest Rate (%):'
  $.loantermLbl.setText 'Loan Term (months): '
  $.loanrateText.setHintText 'Enter'
  $.loanamountText.setHintText 'Enter'
  $.loantermSlider.value = numberMonths
  $.loantermSlider.max = Alloy.Globals.maxterm
  $.loanamountText.focus()
  return
  
changeValue = (e) -> 
  value = new Number(e.source.value)
  switch e.source.id 
    when 'loanamountText'
      if e.source.value isnt ''
        loanAmount = value
        $.loanamountText.value = numeral(value).format('0.00')
        alert "Loan amount is #{numeral(value).format('$0.00')}"
      else 
        Alloy.Globals.showErrorDialog 'You must enter loan amount', $.loanamountText
    when 'loanrateText'
      if e.source.value isnt ''
        interestRate = value
        $.loanrateText.value = numeral(value).format('0.000');
        alert("Interest rate per annum is " + numeral(value / 100).format('0.000%'));
      else 
        Alloy.Globals.showErrorDialog 'You must enter loan rate', $.loanrateText
  return
  
updateLoanTerm = (e) ->
  value = new Number(Math.round(e.source.value))
  numberMonths = value
  $.loantermLbl.text = "Loan Term (#{value} months): "
  return

calculateAndDisplayValue = (e) ->
	Ti.API.info "Clicked button id is #{e.source.id}"
	if not loanAmount?
		Alloy.Globals.showErrorDialog 'You must enter loan amount', $.loanamountText
		return
	totalInterestPaid = (loanAmount * (interestRate / 100) * numberMonths) / 12
	totalRepayments = loanAmount + totalInterestPaid
	if e.source.id is 'calculateLoanRepaymentsBtn'
		msg = "Total repayments is #{numeral(totalRepayments).format('$0.00')}"
	else
		msg = "Total interest paid is #{numeral(totalInterestPaid).format('$0.00')}"		
	Ti.API.info msg
	if Alloy.Globals.autoShowChart is true
		openChartWindow totalInterestPaid, totalRepayments
	else
		resultOptionDialog = Ti.UI.createOptionDialog {
			title: msg + '\n\nDo you want to view this in a chart?'
			options: ['OK','No']
			cancel: 1
		}
		resultOptionDialog.addEventListener 'click', (e) ->
			Ti.API.info "Button index tapped was #{e.index}"
			if e.index is 0
				openChartWindow totalInterestPaid, totalRepayments
				return
		resultOptionDialog.show()
		return


getChartHTML = (totalInterestPaid, totalRepayments) -> 
	principalRepayment = totalRepayments - totalInterestPaid
	chartLoanAmount = "Loan amount is #{numeral(loanAmount).format('$0,0.00')}"
	chartLoanInterest = "Loan rate per annum is #{numeral(interestRate / 100).format('0.000%')}"
	chartLoanTerm = "Loan term is #{numberMonths} months"
	chartInterestPaid = "Total interest paid is #{numeral(totalInterestPaid).format('$0,0.00')}" 
	chartTitleRepayments = "Total repayments is #{numeral(totalRepayments).format('$0,0.00')}"
	chartHTML = """
<html>
	<head> 
	   <script type="text/javascript" src="charts/raphael.js" charset="utf-8"></script> 
	   <script type="text/javascript" src="charts/g.raphael.js" charset="utf-8"></script> 
	   <script type="text/javascript" src="charts/g.pie.js" charset="utf-8"></script> 
	   <script type="text/javascript" charset="utf-8"> 
	       window.onload = function () { 
	         var r = Raphael("chartDiv");
	         r.text(150, 10, "#{chartLoanAmount}").attr({"font-size": 14}); 
	         r.text(150, 40, "#{chartLoanInterest}").attr({"font-size": 14}); 
	         r.text(150, 70, "#{chartLoanTerm}").attr({"font-size": 14});
	         r.text(150, 100, "#{chartInterestPaid}").attr({"font-size": 14}); 
	         r.text(150, 130, "#{chartTitleRepayments}").attr({"font-size": 14}); 
           r.piechart(155, 240, 90, [#{totalInterestPaid.toFixed(2)}, #{principalRepayment.toFixed(2)} ]); 
           };
     </script> 
  </head>
  <body>     
  <div id="chartDiv"></div>
  </body>
</html>
  """
  
openChartWindow = (totalInterestPaid,totalRepayments) -> 
  chartController = Alloy.createController('chart')
  chartHTML = getChartHTML totalInterestPaid,totalRepayments
  chartController.setChartHTML chartHTML
  chartWin = chartController.getView()
  if OS_IOS	
    $.loancalculatorTab.openWindow chartWin
  else if OS_ANDROID
    $.loancalculatorTab.open chartWin
  return

init()
