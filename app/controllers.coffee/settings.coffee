
init = ->
  $.showchartLbl.setText 'Auto Show Chart? '
  $.showchartSwitch.value = Alloy.Globals.autoShowChart
  $.maxtermLbl.setText 'Max Term (months): '
  $.maxtermText.value = Alloy.Globals.maxterm
  return
  
switchChartOption = (e) ->
	Alloy.Globals.autoShowChart = $.showchartSwitch.value
	alert "Option to show chart is set to #{Alloy.Globals.autoShowChart}"

changeValue = (e) ->
#emptyValue = (e.source.value == '' || e.source.value == null)
	value = new Number(e.source.value)
	if e.source.id is 'maxtermText'
		if e.source.value isnt ''
			Alloy.Globals.maxterm = value
			Alloy.Globals.loantermSlider.max = Math.round(value)
			alert "Max term is #{value} months"
		else 
			Alloy.Globals.showErrorDialog 'You must enter max term', $.maxtermText	
	return

init()
