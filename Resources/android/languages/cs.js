(function() {
    var language = {
        delimiters: {
            thousands: " ",
            decimal: ","
        },
        abbreviations: {
            thousand: "tis.",
            million: "mil.",
            billion: "b",
            trillion: "t"
        },
        ordinal: function() {
            return ".";
        },
        currency: {
            symbol: "Kč"
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("cs", language);
})();