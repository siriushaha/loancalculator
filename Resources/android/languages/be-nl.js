(function() {
    var language = {
        delimiters: {
            thousands: " ",
            decimal: ","
        },
        abbreviations: {
            thousand: "k",
            million: " mln",
            billion: " mld",
            trillion: " bln"
        },
        ordinal: function(number) {
            var remainder = number % 100;
            return 0 !== number && 1 >= remainder || 8 === remainder || remainder >= 20 ? "ste" : "de";
        },
        currency: {
            symbol: "€ "
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("be-nl", language);
})();