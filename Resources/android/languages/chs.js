(function() {
    var language = {
        delimiters: {
            thousands: ",",
            decimal: "."
        },
        abbreviations: {
            thousand: "千",
            million: "百万",
            billion: "十亿",
            trillion: "兆"
        },
        ordinal: function() {
            return ".";
        },
        currency: {
            symbol: "¥"
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("chs", language);
})();