(function() {
    var language = {
        delimiters: {
            thousands: ",",
            decimal: "."
        },
        abbreviations: {
            thousand: "พัน",
            million: "ล้าน",
            billion: "พันล้าน",
            trillion: "ล้านล้าน"
        },
        ordinal: function() {
            return ".";
        },
        currency: {
            symbol: "฿"
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("th", language);
})();