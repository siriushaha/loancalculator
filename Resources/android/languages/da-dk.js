(function() {
    var language = {
        delimiters: {
            thousands: ".",
            decimal: ","
        },
        abbreviations: {
            thousand: "k",
            million: "mio",
            billion: "mia",
            trillion: "b"
        },
        ordinal: function() {
            return ".";
        },
        currency: {
            symbol: "DKK"
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("da-dk", language);
})();