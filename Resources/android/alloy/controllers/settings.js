function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "settings";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.settingsWin = Ti.UI.createWindow({
        exitOnclose: true,
        backgroundColor: "white",
        id: "settingsWin",
        title: "Settings"
    });
    $.__views.showchartLbl = Ti.UI.createLabel({
        width: "auto",
        height: 20,
        top: 45,
        left: 20,
        font: {
            fontSize: 14,
            fontFamily: "Helvetica",
            fontWeight: "bold"
        },
        color: "black",
        id: "showchartLbl"
    });
    $.__views.settingsWin.add($.__views.showchartLbl);
    $.__views.showchartSwitch = Ti.UI.createSwitch({
        top: 30,
        right: 20,
        value: false,
        id: "showchartSwitch"
    });
    $.__views.settingsWin.add($.__views.showchartSwitch);
    switchChartOption ? $.__views.showchartSwitch.addEventListener("change", switchChartOption) : __defers["$.__views.showchartSwitch!change!switchChartOption"] = true;
    $.__views.maxtermLbl = Ti.UI.createLabel({
        width: "auto",
        height: 20,
        top: 90,
        left: 20,
        font: {
            fontSize: 14,
            fontFamily: "Helvetica",
            fontWeight: "bold"
        },
        color: "black",
        id: "maxtermLbl"
    });
    $.__views.settingsWin.add($.__views.maxtermLbl);
    $.__views.maxtermText = Ti.UI.createTextField({
        width: 100,
        height: 40,
        top: 80,
        right: 20,
        borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
        returnKeyType: Ti.UI.RETURNKEY_DONE,
        color: "black",
        id: "maxtermText"
    });
    $.__views.settingsWin.add($.__views.maxtermText);
    changeValue ? $.__views.maxtermText.addEventListener("blur", changeValue) : __defers["$.__views.maxtermText!blur!changeValue"] = true;
    changeValue ? $.__views.maxtermText.addEventListener("return", changeValue) : __defers["$.__views.maxtermText!return!changeValue"] = true;
    $.__views.settingsTab = Ti.UI.createTab({
        window: $.__views.settingsWin,
        id: "settingsTab",
        title: "Settings",
        icon: "settings.png"
    });
    $.__views.settingsTab && $.addTopLevelView($.__views.settingsTab);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var changeValue, init, switchChartOption;
    init = function() {
        $.showchartLbl.setText("Auto Show Chart? ");
        $.showchartSwitch.value = Alloy.Globals.autoShowChart;
        $.maxtermLbl.setText("Max Term (months): ");
        $.maxtermText.value = Alloy.Globals.maxterm;
    };
    switchChartOption = function() {
        Alloy.Globals.autoShowChart = $.showchartSwitch.value;
        return alert("Option to show chart is set to " + Alloy.Globals.autoShowChart);
    };
    changeValue = function(e) {
        var value;
        value = new Number(e.source.value);
        if ("maxtermText" === e.source.id) if ("" !== e.source.value) {
            Alloy.Globals.maxterm = value;
            Alloy.Globals.loantermSlider.max = Math.round(value);
            alert("Max term is " + value + " months");
        } else Alloy.Globals.showErrorDialog("You must enter max term", $.maxtermText);
    };
    init();
    __defers["$.__views.showchartSwitch!change!switchChartOption"] && $.__views.showchartSwitch.addEventListener("change", switchChartOption);
    __defers["$.__views.maxtermText!blur!changeValue"] && $.__views.maxtermText.addEventListener("blur", changeValue);
    __defers["$.__views.maxtermText!return!changeValue"] && $.__views.maxtermText.addEventListener("return", changeValue);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;