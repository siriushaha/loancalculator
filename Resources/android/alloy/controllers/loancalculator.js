function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "loancalculator";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.loancalculatorWin = Ti.UI.createWindow({
        exitOnclose: true,
        backgroundColor: "white",
        navBarHidden: false,
        exitOnClose: true,
        id: "loancalculatorWin",
        title: "Loan Calculator"
    });
    $.__views.__alloyId3 = Ti.UI.createView({
        id: "__alloyId3"
    });
    $.__views.loancalculatorWin.add($.__views.__alloyId3);
    $.__views.loanamountLbl = Ti.UI.createLabel({
        width: "auto",
        height: 20,
        top: 50,
        left: 20,
        font: {
            fontSize: 14,
            fontFamily: "Helvetica",
            fontWeight: "bold"
        },
        color: "black",
        id: "loanamountLbl"
    });
    $.__views.__alloyId3.add($.__views.loanamountLbl);
    $.__views.loanamountText = Ti.UI.createTextField({
        width: 100,
        height: 40,
        top: 40,
        right: 20,
        color: "black",
        id: "loanamountText"
    });
    $.__views.__alloyId3.add($.__views.loanamountText);
    changeValue ? $.__views.loanamountText.addEventListener("blur", changeValue) : __defers["$.__views.loanamountText!blur!changeValue"] = true;
    changeValue ? $.__views.loanamountText.addEventListener("return", changeValue) : __defers["$.__views.loanamountText!return!changeValue"] = true;
    $.__views.loanrateLbl = Ti.UI.createLabel({
        width: "auto",
        height: 20,
        top: 80,
        left: 20,
        font: {
            fontSize: 14,
            fontFamily: "Helvetica",
            fontWeight: "bold"
        },
        color: "black",
        id: "loanrateLbl"
    });
    $.__views.__alloyId3.add($.__views.loanrateLbl);
    $.__views.loanrateText = Ti.UI.createTextField({
        width: 100,
        height: 40,
        top: 70,
        right: 20,
        color: "black",
        id: "loanrateText"
    });
    $.__views.__alloyId3.add($.__views.loanrateText);
    changeValue ? $.__views.loanrateText.addEventListener("blur", changeValue) : __defers["$.__views.loanrateText!blur!changeValue"] = true;
    changeValue ? $.__views.loanrateText.addEventListener("return", changeValue) : __defers["$.__views.loanrateText!return!changeValue"] = true;
    $.__views.loantermLbl = Ti.UI.createLabel({
        width: "auto",
        height: 20,
        top: 110,
        left: 20,
        font: {
            fontSize: 14,
            fontFamily: "Helvetica",
            fontWeight: "bold"
        },
        color: "black",
        id: "loantermLbl"
    });
    $.__views.__alloyId3.add($.__views.loantermLbl);
    $.__views.loantermSlider = Ti.UI.createSlider({
        width: 100,
        height: 35,
        top: 110,
        right: 20,
        min: 12,
        max: Alloy.Globals.maxterm,
        id: "loantermSlider"
    });
    $.__views.__alloyId3.add($.__views.loantermSlider);
    updateLoanTerm ? $.__views.loantermSlider.addEventListener("change", updateLoanTerm) : __defers["$.__views.loantermSlider!change!updateLoanTerm"] = true;
    $.__views.calculateInterestPaidBtn = Ti.UI.createButton({
        top: 150,
        width: Ti.UI.SIZE,
        height: 40,
        left: 25,
        color: "black",
        title: "Calculate Interest Paid",
        id: "calculateInterestPaidBtn"
    });
    $.__views.__alloyId3.add($.__views.calculateInterestPaidBtn);
    calculateAndDisplayValue ? $.__views.calculateInterestPaidBtn.addEventListener("click", calculateAndDisplayValue) : __defers["$.__views.calculateInterestPaidBtn!click!calculateAndDisplayValue"] = true;
    $.__views.calculateLoanRepaymentsBtn = Ti.UI.createButton({
        top: 200,
        width: Ti.UI.SIZE,
        height: 40,
        left: 25,
        color: "black",
        title: "Calculate Total Repayments",
        id: "calculateLoanRepaymentsBtn"
    });
    $.__views.__alloyId3.add($.__views.calculateLoanRepaymentsBtn);
    calculateAndDisplayValue ? $.__views.calculateLoanRepaymentsBtn.addEventListener("click", calculateAndDisplayValue) : __defers["$.__views.calculateLoanRepaymentsBtn!click!calculateAndDisplayValue"] = true;
    $.__views.loancalculatorTab = Ti.UI.createTab({
        window: $.__views.loancalculatorWin,
        id: "loancalculatorTab",
        title: "Loan Calculator",
        icon: "calculator.png"
    });
    $.__views.loancalculatorTab && $.addTopLevelView($.__views.loancalculatorTab);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var calculateAndDisplayValue, changeValue, getChartHTML, init, interestRate, loanAmount, numberMonths, numeral, openChartWindow, updateLoanTerm;
    numeral = require("numeral");
    numberMonths = 36;
    interestRate = 6;
    loanAmount = null;
    init = function() {
        Alloy.Globals.loantermSlider = $.loantermSlider;
        $.loanamountLbl.setText("Loan Amount ($):");
        $.loanrateLbl.setText("Interest Rate (%):");
        $.loantermLbl.setText("Loan Term (months): ");
        $.loanrateText.setHintText("Enter");
        $.loanamountText.setHintText("Enter");
        $.loantermSlider.value = numberMonths;
        $.loantermSlider.max = Alloy.Globals.maxterm;
        $.loanamountText.focus();
    };
    changeValue = function(e) {
        var value;
        value = new Number(e.source.value);
        switch (e.source.id) {
          case "loanamountText":
            if ("" !== e.source.value) {
                loanAmount = value;
                $.loanamountText.value = numeral(value).format("0.00");
                alert("Loan amount is " + numeral(value).format("$0.00"));
            } else Alloy.Globals.showErrorDialog("You must enter loan amount", $.loanamountText);
            break;

          case "loanrateText":
            if ("" !== e.source.value) {
                interestRate = value;
                $.loanrateText.value = numeral(value).format("0.000");
                alert("Interest rate per annum is " + numeral(value / 100).format("0.000%"));
            } else Alloy.Globals.showErrorDialog("You must enter loan rate", $.loanrateText);
        }
    };
    updateLoanTerm = function(e) {
        var value;
        value = new Number(Math.round(e.source.value));
        numberMonths = value;
        $.loantermLbl.text = "Loan Term (" + value + " months): ";
    };
    calculateAndDisplayValue = function(e) {
        var msg, resultOptionDialog, totalInterestPaid, totalRepayments;
        Ti.API.info("Clicked button id is " + e.source.id);
        if (null == loanAmount) {
            Alloy.Globals.showErrorDialog("You must enter loan amount", $.loanamountText);
            return;
        }
        totalInterestPaid = loanAmount * (interestRate / 100) * numberMonths / 12;
        totalRepayments = loanAmount + totalInterestPaid;
        msg = "calculateLoanRepaymentsBtn" === e.source.id ? "Total repayments is " + numeral(totalRepayments).format("$0.00") : "Total interest paid is " + numeral(totalInterestPaid).format("$0.00");
        Ti.API.info(msg);
        if (true === Alloy.Globals.autoShowChart) return openChartWindow(totalInterestPaid, totalRepayments);
        resultOptionDialog = Ti.UI.createOptionDialog({
            title: msg + "\n\nDo you want to view this in a chart?",
            options: [ "OK", "No" ],
            cancel: 1
        });
        resultOptionDialog.addEventListener("click", function(e) {
            Ti.API.info("Button index tapped was " + e.index);
            0 === e.index && openChartWindow(totalInterestPaid, totalRepayments);
        });
        resultOptionDialog.show();
    };
    getChartHTML = function(totalInterestPaid, totalRepayments) {
        var chartHTML, chartInterestPaid, chartLoanAmount, chartLoanInterest, chartLoanTerm, chartTitleRepayments, principalRepayment;
        principalRepayment = totalRepayments - totalInterestPaid;
        chartLoanAmount = "Loan amount is " + numeral(loanAmount).format("$0,0.00");
        chartLoanInterest = "Loan rate per annum is " + numeral(interestRate / 100).format("0.000%");
        chartLoanTerm = "Loan term is " + numberMonths + " months";
        chartInterestPaid = "Total interest paid is " + numeral(totalInterestPaid).format("$0,0.00");
        chartTitleRepayments = "Total repayments is " + numeral(totalRepayments).format("$0,0.00");
        return chartHTML = '<html>\n	<head> \n	   <script type="text/javascript" src="charts/raphael.js" charset="utf-8"></script> \n	   <script type="text/javascript" src="charts/g.raphael.js" charset="utf-8"></script> \n	   <script type="text/javascript" src="charts/g.pie.js" charset="utf-8"></script> \n	   <script type="text/javascript" charset="utf-8"> \n	       window.onload = function () { \n	         var r = Raphael("chartDiv");\n	         r.text(150, 10, "' + chartLoanAmount + '").attr({"font-size": 14}); \n	         r.text(150, 40, "' + chartLoanInterest + '").attr({"font-size": 14}); \n	         r.text(150, 70, "' + chartLoanTerm + '").attr({"font-size": 14});\n	         r.text(150, 100, "' + chartInterestPaid + '").attr({"font-size": 14}); \n	         r.text(150, 130, "' + chartTitleRepayments + '").attr({"font-size": 14}); \n           r.piechart(155, 240, 90, [' + totalInterestPaid.toFixed(2) + ", " + principalRepayment.toFixed(2) + ' ]); \n           };\n     </script> \n  </head>\n  <body>     \n  <div id="chartDiv"></div>\n  </body>\n</html>';
    };
    openChartWindow = function(totalInterestPaid, totalRepayments) {
        var chartController, chartHTML, chartWin;
        chartController = Alloy.createController("chart");
        chartHTML = getChartHTML(totalInterestPaid, totalRepayments);
        chartController.setChartHTML(chartHTML);
        chartWin = chartController.getView();
        $.loancalculatorTab.open(chartWin);
    };
    init();
    __defers["$.__views.loanamountText!blur!changeValue"] && $.__views.loanamountText.addEventListener("blur", changeValue);
    __defers["$.__views.loanamountText!return!changeValue"] && $.__views.loanamountText.addEventListener("return", changeValue);
    __defers["$.__views.loanrateText!blur!changeValue"] && $.__views.loanrateText.addEventListener("blur", changeValue);
    __defers["$.__views.loanrateText!return!changeValue"] && $.__views.loanrateText.addEventListener("return", changeValue);
    __defers["$.__views.loantermSlider!change!updateLoanTerm"] && $.__views.loantermSlider.addEventListener("change", updateLoanTerm);
    __defers["$.__views.calculateInterestPaidBtn!click!calculateAndDisplayValue"] && $.__views.calculateInterestPaidBtn.addEventListener("click", calculateAndDisplayValue);
    __defers["$.__views.calculateLoanRepaymentsBtn!click!calculateAndDisplayValue"] && $.__views.calculateLoanRepaymentsBtn.addEventListener("click", calculateAndDisplayValue);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;