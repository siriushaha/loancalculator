function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "chart";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    $.__views.chartWin = Ti.UI.createWindow({
        exitOnclose: true,
        title: "Loan Pie Chart",
        id: "chartWin"
    });
    $.__views.chartWin && $.addTopLevelView($.__views.chartWin);
    $.__views.chartView = Ti.UI.createWebView({
        top: 0,
        id: "chartView"
    });
    $.__views.chartWin.add($.__views.chartView);
    exports.destroy = function() {};
    _.extend($, $.__views);
    exports.setChartHTML = function(chartHTML) {
        $.chartView.html = chartHTML;
        Ti.API.info(chartHTML);
    };
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;