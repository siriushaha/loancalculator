function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "loancalculator";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.loancalculatorWin = Ti.UI.createWindow({
        id: "loancalculatorWin",
        title: "Loan Calculator"
    });
    $.__views.__alloyId2 = Ti.UI.createView({
        id: "__alloyId2"
    });
    $.__views.loancalculatorWin.add($.__views.__alloyId2);
    $.__views.__alloyId3 = Ti.UI.createLabel({
        text: "in Loan Calculator window",
        id: "__alloyId3"
    });
    $.__views.loancalculatorWin.add($.__views.__alloyId3);
    $.__views.loancalculatorTab = Ti.UI.createTab({
        window: $.__views.loancalculatorWin,
        id: "loancalculatorTab",
        title: "Loan Calculator",
        icon: "calculator.png"
    });
    $.__views.loancalculatorTab && $.addTopLevelView($.__views.loancalculatorTab);
    exports.destroy = function() {};
    _.extend($, $.__views);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;