function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "settings";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.settingsWin = Ti.UI.createWindow({
        backgroundColor: "white",
        id: "settingsWin",
        title: "Settings"
    });
    $.__views.__alloyId4 = Ti.UI.createLabel({
        text: "in Settings window",
        id: "__alloyId4"
    });
    $.__views.settingsWin.add($.__views.__alloyId4);
    $.__views.settingsTab = Ti.UI.createTab({
        window: $.__views.settingsWin,
        id: "settingsTab",
        title: "Settings",
        icon: "settings.png"
    });
    $.__views.settingsTab && $.addTopLevelView($.__views.settingsTab);
    exports.destroy = function() {};
    _.extend($, $.__views);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;