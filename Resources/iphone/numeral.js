(function() {
    function Numeral(number) {
        this._value = number;
    }
    function toFixed(value, precision, roundingFunction, optionals) {
        var optionalsRegExp, output, power = Math.pow(10, precision);
        output = (roundingFunction(value * power) / power).toFixed(precision);
        if (optionals) {
            optionalsRegExp = new RegExp("0{1," + optionals + "}$");
            output = output.replace(optionalsRegExp, "");
        }
        return output;
    }
    function formatNumeral(n, format, roundingFunction) {
        var output;
        output = format.indexOf("$") > -1 ? formatCurrency(n, format, roundingFunction) : format.indexOf("%") > -1 ? formatPercentage(n, format, roundingFunction) : format.indexOf(":") > -1 ? formatTime(n, format) : formatNumber(n._value, format, roundingFunction);
        return output;
    }
    function unformatNumeral(n, string) {
        var thousandRegExp, millionRegExp, billionRegExp, trillionRegExp, power, stringOriginal = string, suffixes = [ "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" ], bytesMultiplier = false;
        if (string.indexOf(":") > -1) n._value = unformatTime(string); else if (string === zeroFormat) n._value = 0; else {
            "." !== languages[currentLanguage].delimiters.decimal && (string = string.replace(/\./g, "").replace(languages[currentLanguage].delimiters.decimal, "."));
            thousandRegExp = new RegExp("[^a-zA-Z]" + languages[currentLanguage].abbreviations.thousand + "(?:\\)|(\\" + languages[currentLanguage].currency.symbol + ")?(?:\\))?)?$");
            millionRegExp = new RegExp("[^a-zA-Z]" + languages[currentLanguage].abbreviations.million + "(?:\\)|(\\" + languages[currentLanguage].currency.symbol + ")?(?:\\))?)?$");
            billionRegExp = new RegExp("[^a-zA-Z]" + languages[currentLanguage].abbreviations.billion + "(?:\\)|(\\" + languages[currentLanguage].currency.symbol + ")?(?:\\))?)?$");
            trillionRegExp = new RegExp("[^a-zA-Z]" + languages[currentLanguage].abbreviations.trillion + "(?:\\)|(\\" + languages[currentLanguage].currency.symbol + ")?(?:\\))?)?$");
            for (power = 0; suffixes.length >= power; power++) {
                bytesMultiplier = string.indexOf(suffixes[power]) > -1 ? Math.pow(1024, power + 1) : false;
                if (bytesMultiplier) break;
            }
            n._value = (bytesMultiplier ? bytesMultiplier : 1) * (stringOriginal.match(thousandRegExp) ? Math.pow(10, 3) : 1) * (stringOriginal.match(millionRegExp) ? Math.pow(10, 6) : 1) * (stringOriginal.match(billionRegExp) ? Math.pow(10, 9) : 1) * (stringOriginal.match(trillionRegExp) ? Math.pow(10, 12) : 1) * (string.indexOf("%") > -1 ? .01 : 1) * ((string.split("-").length + Math.min(string.split("(").length - 1, string.split(")").length - 1)) % 2 ? 1 : -1) * Number(string.replace(/[^0-9\.]+/g, ""));
            n._value = bytesMultiplier ? Math.ceil(n._value) : n._value;
        }
        return n._value;
    }
    function formatCurrency(n, format, roundingFunction) {
        var spliceIndex, output, symbolIndex = format.indexOf("$"), openParenIndex = format.indexOf("("), minusSignIndex = format.indexOf("-"), space = "";
        if (format.indexOf(" $") > -1) {
            space = " ";
            format = format.replace(" $", "");
        } else if (format.indexOf("$ ") > -1) {
            space = " ";
            format = format.replace("$ ", "");
        } else format = format.replace("$", "");
        output = formatNumber(n._value, format, roundingFunction);
        if (1 >= symbolIndex) if (output.indexOf("(") > -1 || output.indexOf("-") > -1) {
            output = output.split("");
            spliceIndex = 1;
            (openParenIndex > symbolIndex || minusSignIndex > symbolIndex) && (spliceIndex = 0);
            output.splice(spliceIndex, 0, languages[currentLanguage].currency.symbol + space);
            output = output.join("");
        } else output = languages[currentLanguage].currency.symbol + space + output; else if (output.indexOf(")") > -1) {
            output = output.split("");
            output.splice(-1, 0, space + languages[currentLanguage].currency.symbol);
            output = output.join("");
        } else output = output + space + languages[currentLanguage].currency.symbol;
        return output;
    }
    function formatPercentage(n, format, roundingFunction) {
        var output, space = "", value = 100 * n._value;
        if (format.indexOf(" %") > -1) {
            space = " ";
            format = format.replace(" %", "");
        } else format = format.replace("%", "");
        output = formatNumber(value, format, roundingFunction);
        if (output.indexOf(")") > -1) {
            output = output.split("");
            output.splice(-1, 0, space + "%");
            output = output.join("");
        } else output = output + space + "%";
        return output;
    }
    function formatTime(n) {
        var hours = Math.floor(n._value / 60 / 60), minutes = Math.floor((n._value - 60 * 60 * hours) / 60), seconds = Math.round(n._value - 60 * 60 * hours - 60 * minutes);
        return hours + ":" + (10 > minutes ? "0" + minutes : minutes) + ":" + (10 > seconds ? "0" + seconds : seconds);
    }
    function unformatTime(string) {
        var timeArray = string.split(":"), seconds = 0;
        if (3 === timeArray.length) {
            seconds += 60 * 60 * Number(timeArray[0]);
            seconds += 60 * Number(timeArray[1]);
            seconds += Number(timeArray[2]);
        } else if (2 === timeArray.length) {
            seconds += 60 * Number(timeArray[0]);
            seconds += Number(timeArray[1]);
        }
        return Number(seconds);
    }
    function formatNumber(value, format, roundingFunction) {
        var min, max, power, w, precision, thousands, negP = false, signed = false, optDec = false, abbr = "", abbrK = false, abbrM = false, abbrB = false, abbrT = false, abbrForce = false, bytes = "", ord = "", abs = Math.abs(value), suffixes = [ "B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" ], d = "", neg = false;
        if (0 === value && null !== zeroFormat) return zeroFormat;
        if (format.indexOf("(") > -1) {
            negP = true;
            format = format.slice(1, -1);
        } else if (format.indexOf("+") > -1) {
            signed = true;
            format = format.replace(/\+/g, "");
        }
        if (format.indexOf("a") > -1) {
            abbrK = format.indexOf("aK") >= 0;
            abbrM = format.indexOf("aM") >= 0;
            abbrB = format.indexOf("aB") >= 0;
            abbrT = format.indexOf("aT") >= 0;
            abbrForce = abbrK || abbrM || abbrB || abbrT;
            if (format.indexOf(" a") > -1) {
                abbr = " ";
                format = format.replace(" a", "");
            } else format = format.replace("a", "");
            if (abs >= Math.pow(10, 12) && !abbrForce || abbrT) {
                abbr += languages[currentLanguage].abbreviations.trillion;
                value /= Math.pow(10, 12);
            } else if (Math.pow(10, 12) > abs && abs >= Math.pow(10, 9) && !abbrForce || abbrB) {
                abbr += languages[currentLanguage].abbreviations.billion;
                value /= Math.pow(10, 9);
            } else if (Math.pow(10, 9) > abs && abs >= Math.pow(10, 6) && !abbrForce || abbrM) {
                abbr += languages[currentLanguage].abbreviations.million;
                value /= Math.pow(10, 6);
            } else if (Math.pow(10, 6) > abs && abs >= Math.pow(10, 3) && !abbrForce || abbrK) {
                abbr += languages[currentLanguage].abbreviations.thousand;
                value /= Math.pow(10, 3);
            }
        }
        if (format.indexOf("b") > -1) {
            if (format.indexOf(" b") > -1) {
                bytes = " ";
                format = format.replace(" b", "");
            } else format = format.replace("b", "");
            for (power = 0; suffixes.length >= power; power++) {
                min = Math.pow(1024, power);
                max = Math.pow(1024, power + 1);
                if (value >= min && max > value) {
                    bytes += suffixes[power];
                    min > 0 && (value /= min);
                    break;
                }
            }
        }
        if (format.indexOf("o") > -1) {
            if (format.indexOf(" o") > -1) {
                ord = " ";
                format = format.replace(" o", "");
            } else format = format.replace("o", "");
            ord += languages[currentLanguage].ordinal(value);
        }
        if (format.indexOf("[.]") > -1) {
            optDec = true;
            format = format.replace("[.]", ".");
        }
        w = value.toString().split(".")[0];
        precision = format.split(".")[1];
        thousands = format.indexOf(",");
        if (precision) {
            if (precision.indexOf("[") > -1) {
                precision = precision.replace("]", "");
                precision = precision.split("[");
                d = toFixed(value, precision[0].length + precision[1].length, roundingFunction, precision[1].length);
            } else d = toFixed(value, precision.length, roundingFunction);
            w = d.split(".")[0];
            d = d.split(".")[1].length ? languages[currentLanguage].delimiters.decimal + d.split(".")[1] : "";
            optDec && 0 === Number(d.slice(1)) && (d = "");
        } else w = toFixed(value, null, roundingFunction);
        if (w.indexOf("-") > -1) {
            w = w.slice(1);
            neg = true;
        }
        thousands > -1 && (w = w.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1" + languages[currentLanguage].delimiters.thousands));
        0 === format.indexOf(".") && (w = "");
        return (negP && neg ? "(" : "") + (!negP && neg ? "-" : "") + (!neg && signed ? "+" : "") + w + d + (ord ? ord : "") + (abbr ? abbr : "") + (bytes ? bytes : "") + (negP && neg ? ")" : "");
    }
    function loadLanguage(key, values) {
        languages[key] = values;
    }
    function multiplier(x) {
        var parts = x.toString().split(".");
        if (2 > parts.length) return 1;
        return Math.pow(10, parts[1].length);
    }
    function correctionFactor() {
        var args = Array.prototype.slice.call(arguments);
        return args.reduce(function(prev, next) {
            var mp = multiplier(prev), mn = multiplier(next);
            return mp > mn ? mp : mn;
        }, -1/0);
    }
    var numeral, VERSION = "1.5.3", languages = {}, currentLanguage = "en", zeroFormat = null, defaultFormat = "0,0", hasModule = "undefined" != typeof module && module.exports;
    numeral = function(input) {
        numeral.isNumeral(input) ? input = input.value() : 0 === input || "undefined" == typeof input ? input = 0 : Number(input) || (input = numeral.fn.unformat(input));
        return new Numeral(Number(input));
    };
    numeral.version = VERSION;
    numeral.isNumeral = function(obj) {
        return obj instanceof Numeral;
    };
    numeral.language = function(key, values) {
        if (!key) return currentLanguage;
        if (key && !values) {
            if (!languages[key]) throw new Error("Unknown language : " + key);
            currentLanguage = key;
        }
        (values || !languages[key]) && loadLanguage(key, values);
        return numeral;
    };
    numeral.languageData = function(key) {
        if (!key) return languages[currentLanguage];
        if (!languages[key]) throw new Error("Unknown language : " + key);
        return languages[key];
    };
    numeral.language("en", {
        delimiters: {
            thousands: ",",
            decimal: "."
        },
        abbreviations: {
            thousand: "k",
            million: "m",
            billion: "b",
            trillion: "t"
        },
        ordinal: function(number) {
            var b = number % 10;
            return 1 === ~~(number % 100 / 10) ? "th" : 1 === b ? "st" : 2 === b ? "nd" : 3 === b ? "rd" : "th";
        },
        currency: {
            symbol: "$"
        }
    });
    numeral.zeroFormat = function(format) {
        zeroFormat = "string" == typeof format ? format : null;
    };
    numeral.defaultFormat = function(format) {
        defaultFormat = "string" == typeof format ? format : "0.0";
    };
    "function" != typeof Array.prototype.reduce && (Array.prototype.reduce = function(callback, opt_initialValue) {
        "use strict";
        if (null === this || "undefined" == typeof this) throw new TypeError("Array.prototype.reduce called on null or undefined");
        if ("function" != typeof callback) throw new TypeError(callback + " is not a function");
        var index, value, length = this.length >>> 0, isValueSet = false;
        if (arguments.length > 1) {
            value = opt_initialValue;
            isValueSet = true;
        }
        for (index = 0; length > index; ++index) if (this.hasOwnProperty(index)) if (isValueSet) value = callback(value, this[index], index, this); else {
            value = this[index];
            isValueSet = true;
        }
        if (!isValueSet) throw new TypeError("Reduce of empty array with no initial value");
        return value;
    });
    numeral.fn = Numeral.prototype = {
        clone: function() {
            return numeral(this);
        },
        format: function(inputString, roundingFunction) {
            return formatNumeral(this, inputString ? inputString : defaultFormat, void 0 !== roundingFunction ? roundingFunction : Math.round);
        },
        unformat: function(inputString) {
            if ("[object Number]" === Object.prototype.toString.call(inputString)) return inputString;
            return unformatNumeral(this, inputString ? inputString : defaultFormat);
        },
        value: function() {
            return this._value;
        },
        valueOf: function() {
            return this._value;
        },
        set: function(value) {
            this._value = Number(value);
            return this;
        },
        add: function(value) {
            function cback(accum, curr) {
                return accum + corrFactor * curr;
            }
            var corrFactor = correctionFactor.call(null, this._value, value);
            this._value = [ this._value, value ].reduce(cback, 0) / corrFactor;
            return this;
        },
        subtract: function(value) {
            function cback(accum, curr) {
                return accum - corrFactor * curr;
            }
            var corrFactor = correctionFactor.call(null, this._value, value);
            this._value = [ value ].reduce(cback, this._value * corrFactor) / corrFactor;
            return this;
        },
        multiply: function(value) {
            function cback(accum, curr) {
                var corrFactor = correctionFactor(accum, curr);
                return accum * corrFactor * curr * corrFactor / (corrFactor * corrFactor);
            }
            this._value = [ this._value, value ].reduce(cback, 1);
            return this;
        },
        divide: function(value) {
            function cback(accum, curr) {
                var corrFactor = correctionFactor(accum, curr);
                return accum * corrFactor / (curr * corrFactor);
            }
            this._value = [ this._value, value ].reduce(cback);
            return this;
        },
        difference: function(value) {
            return Math.abs(numeral(this._value).subtract(value).value());
        }
    };
    hasModule && (module.exports = numeral);
    "undefined" == typeof ender && (this["numeral"] = numeral);
    "function" == typeof define && define.amd && define([], function() {
        return numeral;
    });
}).call(this);