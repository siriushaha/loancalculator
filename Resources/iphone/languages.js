(function() {
    var language = {
        delimiters: {
            thousands: " ",
            decimal: ","
        },
        abbreviations: {
            thousand: "k",
            million: " mln",
            billion: " mld",
            trillion: " bln"
        },
        ordinal: function(number) {
            var remainder = number % 100;
            return 0 !== number && 1 >= remainder || 8 === remainder || remainder >= 20 ? "ste" : "de";
        },
        currency: {
            symbol: "€ "
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("be-nl", language);
})();

(function() {
    var language = {
        delimiters: {
            thousands: ",",
            decimal: "."
        },
        abbreviations: {
            thousand: "千",
            million: "百万",
            billion: "十亿",
            trillion: "兆"
        },
        ordinal: function() {
            return ".";
        },
        currency: {
            symbol: "¥"
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("chs", language);
})();

(function() {
    var language = {
        delimiters: {
            thousands: " ",
            decimal: ","
        },
        abbreviations: {
            thousand: "tis.",
            million: "mil.",
            billion: "b",
            trillion: "t"
        },
        ordinal: function() {
            return ".";
        },
        currency: {
            symbol: "Kč"
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("cs", language);
})();

(function() {
    var language = {
        delimiters: {
            thousands: ".",
            decimal: ","
        },
        abbreviations: {
            thousand: "k",
            million: "mio",
            billion: "mia",
            trillion: "b"
        },
        ordinal: function() {
            return ".";
        },
        currency: {
            symbol: "DKK"
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("da-dk", language);
})();

(function() {
    var language = {
        delimiters: {
            thousands: " ",
            decimal: ","
        },
        abbreviations: {
            thousand: "k",
            million: "m",
            billion: "b",
            trillion: "t"
        },
        ordinal: function() {
            return ".";
        },
        currency: {
            symbol: "CHF"
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("de-ch", language);
})();

(function() {
    var language = {
        delimiters: {
            thousands: " ",
            decimal: ","
        },
        abbreviations: {
            thousand: "k",
            million: "m",
            billion: "b",
            trillion: "t"
        },
        ordinal: function() {
            return ".";
        },
        currency: {
            symbol: "€"
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("de", language);
})();

(function() {
    var language = {
        delimiters: {
            thousands: ",",
            decimal: "."
        },
        abbreviations: {
            thousand: "k",
            million: "m",
            billion: "b",
            trillion: "t"
        },
        ordinal: function(number) {
            var b = number % 10;
            return 1 === ~~(number % 100 / 10) ? "th" : 1 === b ? "st" : 2 === b ? "nd" : 3 === b ? "rd" : "th";
        },
        currency: {
            symbol: "£"
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("en-gb", language);
})();

(function() {
    var language = {
        delimiters: {
            thousands: ".",
            decimal: ","
        },
        abbreviations: {
            thousand: "k",
            million: "mm",
            billion: "b",
            trillion: "t"
        },
        ordinal: function(number) {
            var b = number % 10;
            return 1 === b || 3 === b ? "er" : 2 === b ? "do" : 7 === b || 0 === b ? "mo" : 8 === b ? "vo" : 9 === b ? "no" : "to";
        },
        currency: {
            symbol: "€"
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("es", language);
})();

(function() {
    var language = {
        delimiters: {
            thousands: ".",
            decimal: ","
        },
        abbreviations: {
            thousand: "k",
            million: "mm",
            billion: "b",
            trillion: "t"
        },
        ordinal: function(number) {
            var b = number % 10;
            return 1 === b || 3 === b ? "er" : 2 === b ? "do" : 7 === b || 0 === b ? "mo" : 8 === b ? "vo" : 9 === b ? "no" : "to";
        },
        currency: {
            symbol: "$"
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("es", language);
})();

(function() {
    var language = {
        delimiters: {
            thousands: " ",
            decimal: ","
        },
        abbreviations: {
            thousand: " tuh",
            million: " mln",
            billion: " mld",
            trillion: " trl"
        },
        ordinal: function() {
            return ".";
        },
        currency: {
            symbol: "€"
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("et", language);
})();

(function() {
    var language = {
        delimiters: {
            thousands: " ",
            decimal: ","
        },
        abbreviations: {
            thousand: "k",
            million: "M",
            billion: "G",
            trillion: "T"
        },
        ordinal: function() {
            return ".";
        },
        currency: {
            symbol: "€"
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("fi", language);
})();

(function() {
    var language = {
        delimiters: {
            thousands: " ",
            decimal: ","
        },
        abbreviations: {
            thousand: "k",
            million: "M",
            billion: "G",
            trillion: "T"
        },
        ordinal: function(number) {
            return 1 === number ? "er" : "e";
        },
        currency: {
            symbol: "$"
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("fr-CA", language);
})();

(function() {
    var language = {
        delimiters: {
            thousands: "'",
            decimal: "."
        },
        abbreviations: {
            thousand: "k",
            million: "m",
            billion: "b",
            trillion: "t"
        },
        ordinal: function(number) {
            return 1 === number ? "er" : "e";
        },
        currency: {
            symbol: "CHF"
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("fr-ch", language);
})();

(function() {
    var language = {
        delimiters: {
            thousands: " ",
            decimal: ","
        },
        abbreviations: {
            thousand: "k",
            million: "m",
            billion: "b",
            trillion: "t"
        },
        ordinal: function(number) {
            return 1 === number ? "er" : "e";
        },
        currency: {
            symbol: "€"
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("fr", language);
})();

(function() {
    var language = {
        delimiters: {
            thousands: " ",
            decimal: ","
        },
        abbreviations: {
            thousand: "E",
            million: "M",
            billion: "Mrd",
            trillion: "T"
        },
        ordinal: function() {
            return ".";
        },
        currency: {
            symbol: " Ft"
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("hu", language);
})();

(function() {
    var language = {
        delimiters: {
            thousands: ".",
            decimal: ","
        },
        abbreviations: {
            thousand: "mila",
            million: "mil",
            billion: "b",
            trillion: "t"
        },
        ordinal: function() {
            return "º";
        },
        currency: {
            symbol: "€"
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("it", language);
})();

(function() {
    var language = {
        delimiters: {
            thousands: ",",
            decimal: "."
        },
        abbreviations: {
            thousand: "千",
            million: "百万",
            billion: "十億",
            trillion: "兆"
        },
        ordinal: function() {
            return ".";
        },
        currency: {
            symbol: "¥"
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("ja", language);
})();

(function() {
    var language = {
        delimiters: {
            thousands: ".",
            decimal: ","
        },
        abbreviations: {
            thousand: "k",
            million: "mln",
            billion: "mrd",
            trillion: "bln"
        },
        ordinal: function(number) {
            var remainder = number % 100;
            return 0 !== number && 1 >= remainder || 8 === remainder || remainder >= 20 ? "ste" : "de";
        },
        currency: {
            symbol: "€ "
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("nl-nl", language);
})();

(function() {
    var language = {
        delimiters: {
            thousands: " ",
            decimal: ","
        },
        abbreviations: {
            thousand: "tys.",
            million: "mln",
            billion: "mld",
            trillion: "bln"
        },
        ordinal: function() {
            return ".";
        },
        currency: {
            symbol: "PLN"
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("pl", language);
})();

(function() {
    var language = {
        delimiters: {
            thousands: ".",
            decimal: ","
        },
        abbreviations: {
            thousand: "mil",
            million: "milhões",
            billion: "b",
            trillion: "t"
        },
        ordinal: function() {
            return "º";
        },
        currency: {
            symbol: "R$"
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("pt-br", language);
})();

(function() {
    var language = {
        delimiters: {
            thousands: " ",
            decimal: ","
        },
        abbreviations: {
            thousand: "k",
            million: "m",
            billion: "b",
            trillion: "t"
        },
        ordinal: function() {
            return "º";
        },
        currency: {
            symbol: "€"
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("pt-pt", language);
})();

(function() {
    var language = {
        delimiters: {
            thousands: " ",
            decimal: ","
        },
        abbreviations: {
            thousand: "тыс.",
            million: "млн",
            billion: "b",
            trillion: "t"
        },
        ordinal: function() {
            return ".";
        },
        currency: {
            symbol: "₴"
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("ru-UA", language);
})();

(function() {
    var language = {
        delimiters: {
            thousands: " ",
            decimal: ","
        },
        abbreviations: {
            thousand: "тыс.",
            million: "млн",
            billion: "b",
            trillion: "t"
        },
        ordinal: function() {
            return ".";
        },
        currency: {
            symbol: "руб."
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("ru", language);
})();

(function() {
    var language = {
        delimiters: {
            thousands: " ",
            decimal: ","
        },
        abbreviations: {
            thousand: "tis.",
            million: "mil.",
            billion: "b",
            trillion: "t"
        },
        ordinal: function() {
            return ".";
        },
        currency: {
            symbol: "€"
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("sk", language);
})();

(function() {
    var language = {
        delimiters: {
            thousands: ",",
            decimal: "."
        },
        abbreviations: {
            thousand: "พัน",
            million: "ล้าน",
            billion: "พันล้าน",
            trillion: "ล้านล้าน"
        },
        ordinal: function() {
            return ".";
        },
        currency: {
            symbol: "฿"
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("th", language);
})();

(function() {
    var suffixes = {
        1: "'inci",
        5: "'inci",
        8: "'inci",
        70: "'inci",
        80: "'inci",
        2: "'nci",
        7: "'nci",
        20: "'nci",
        50: "'nci",
        3: "'üncü",
        4: "'üncü",
        100: "'üncü",
        6: "'ncı",
        9: "'uncu",
        10: "'uncu",
        30: "'uncu",
        60: "'ıncı",
        90: "'ıncı"
    }, language = {
        delimiters: {
            thousands: ".",
            decimal: ","
        },
        abbreviations: {
            thousand: "bin",
            million: "milyon",
            billion: "milyar",
            trillion: "trilyon"
        },
        ordinal: function(number) {
            if (0 === number) return "'ıncı";
            var a = number % 10, b = number % 100 - a, c = number >= 100 ? 100 : null;
            return suffixes[a] || suffixes[b] || suffixes[c];
        },
        currency: {
            symbol: "₺"
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("tr", language);
})();

(function() {
    var language = {
        delimiters: {
            thousands: " ",
            decimal: ","
        },
        abbreviations: {
            thousand: "тис.",
            million: "млн",
            billion: "млрд",
            trillion: "блн"
        },
        ordinal: function() {
            return "";
        },
        currency: {
            symbol: "₴"
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("uk-UA", language);
})();