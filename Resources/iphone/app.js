var Alloy = require("alloy"), _ = Alloy._, Backbone = Alloy.Backbone;

Alloy.Globals.maxterm = 60;

Alloy.Globals.autoShowChart = false;

Alloy.Globals.showErrorDialog = function(msg, entry) {
    var errorDialog = Ti.UI.createAlertDialog({
        title: "Error",
        message: msg
    });
    errorDialog.show();
    entry.focus();
};

Alloy.createController("index");