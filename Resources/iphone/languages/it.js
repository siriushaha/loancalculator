(function() {
    var language = {
        delimiters: {
            thousands: ".",
            decimal: ","
        },
        abbreviations: {
            thousand: "mila",
            million: "mil",
            billion: "b",
            trillion: "t"
        },
        ordinal: function() {
            return "º";
        },
        currency: {
            symbol: "€"
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("it", language);
})();