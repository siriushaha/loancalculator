(function() {
    var language = {
        delimiters: {
            thousands: " ",
            decimal: ","
        },
        abbreviations: {
            thousand: "E",
            million: "M",
            billion: "Mrd",
            trillion: "T"
        },
        ordinal: function() {
            return ".";
        },
        currency: {
            symbol: " Ft"
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("hu", language);
})();