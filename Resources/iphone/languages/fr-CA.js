(function() {
    var language = {
        delimiters: {
            thousands: " ",
            decimal: ","
        },
        abbreviations: {
            thousand: "k",
            million: "M",
            billion: "G",
            trillion: "T"
        },
        ordinal: function(number) {
            return 1 === number ? "er" : "e";
        },
        currency: {
            symbol: "$"
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("fr-CA", language);
})();