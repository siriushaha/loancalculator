(function() {
    var language = {
        delimiters: {
            thousands: " ",
            decimal: ","
        },
        abbreviations: {
            thousand: "тыс.",
            million: "млн",
            billion: "b",
            trillion: "t"
        },
        ordinal: function() {
            return ".";
        },
        currency: {
            symbol: "₴"
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("ru-UA", language);
})();