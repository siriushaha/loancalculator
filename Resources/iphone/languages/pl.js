(function() {
    var language = {
        delimiters: {
            thousands: " ",
            decimal: ","
        },
        abbreviations: {
            thousand: "tys.",
            million: "mln",
            billion: "mld",
            trillion: "bln"
        },
        ordinal: function() {
            return ".";
        },
        currency: {
            symbol: "PLN"
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("pl", language);
})();