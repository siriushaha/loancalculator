(function() {
    var language = {
        delimiters: {
            thousands: " ",
            decimal: ","
        },
        abbreviations: {
            thousand: "тис.",
            million: "млн",
            billion: "млрд",
            trillion: "блн"
        },
        ordinal: function() {
            return "";
        },
        currency: {
            symbol: "₴"
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("uk-UA", language);
})();