(function() {
    var language = {
        delimiters: {
            thousands: ".",
            decimal: ","
        },
        abbreviations: {
            thousand: "mil",
            million: "milhões",
            billion: "b",
            trillion: "t"
        },
        ordinal: function() {
            return "º";
        },
        currency: {
            symbol: "R$"
        }
    };
    "undefined" != typeof module && module.exports && (module.exports = language);
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("pt-br", language);
})();